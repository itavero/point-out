import CpplintParser from '../../src/parsers/cpplint_parser';
import {FileReport, LineSource, ReportSeverity} from '../../src/parser';

describe('CpplintParser', () => {
  let parser: CpplintParser;
  let fileReport: FileReport;

  beforeEach(() => {
    parser = new CpplintParser();
    fileReport = jest.fn();
  });

  it('files a report when a line has a cpplint warning (with relative path)', () => {
    const line =
      'main/src/main.cpp:166:  Is this a non-const reference? If so, make const or use a pointer: my::namespace::SomeType& attributes  [runtime/references] [2]';

    parser.parseLine(line, LineSource.stderr, fileReport);

    expect(fileReport).toHaveBeenCalledWith({
      severity: ReportSeverity.Info,
      description:
        'Is this a non-const reference? If so, make const or use a pointer: my::namespace::SomeType& attributes [runtime/references]',
      line: 166,
      file: 'main/src/main.cpp',
    });
  });

  it('files a report when a line has a cpplint warning (with absolute Windows-style path)', () => {
    const line =
      'C:\\path\\to\\file.cpp:10:  This is a warning message  [category] [3]';

    parser.parseLine(line, LineSource.stderr, fileReport);

    expect(fileReport).toHaveBeenCalledWith({
      severity: ReportSeverity.Minor,
      description: 'This is a warning message [category]',
      line: 10,
      file: 'C:\\path\\to\\file.cpp',
    });
  });

  it('files a report when a line has a cpplint warning (with absolute Unix-style path)', () => {
    const line =
      '/path/to/file.cpp:10:  This is a warning message  [category] [4]';

    parser.parseLine(line, LineSource.stderr, fileReport);

    expect(fileReport).toHaveBeenCalledWith({
      severity: ReportSeverity.Major,
      description: 'This is a warning message [category]',
      line: 10,
      file: '/path/to/file.cpp',
    });
  });
});
