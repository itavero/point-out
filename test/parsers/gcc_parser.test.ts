import GccParser from '../../src/parsers/gcc_parser';
import {FileReport, LineSource, ReportSeverity} from '../../src/parser';

describe('GccParser', () => {
  let gccParser: GccParser;
  let fileReport: FileReport;

  beforeEach(() => {
    gccParser = new GccParser();
    fileReport = jest.fn();
  });

  it('files a report when a line has a GCC warning (with Unix path)', () => {
    const line = '/path/to/file.cpp:10:5: warning: This is a warning message';

    gccParser.parseLine(line, LineSource.stderr, fileReport);

    expect(fileReport).toHaveBeenCalledWith({
      severity: ReportSeverity.Minor,
      description: 'This is a warning message',
      line: 10,
      file: '/path/to/file.cpp',
    });
  });

  it('files a report when a line has a GCC warning (with Windows path)', () => {
    const line =
      'C:\\path\\to\\file.cpp:10:5: warning: This is a warning message';

    gccParser.parseLine(line, LineSource.stderr, fileReport);

    expect(fileReport).toHaveBeenCalledWith({
      severity: ReportSeverity.Minor,
      description: 'This is a warning message',
      line: 10,
      file: 'C:\\path\\to\\file.cpp',
    });
  });

  it('files a report when a line has a GCC warning (with Windows path and forward slashes)', () => {
    const line = 'C:/path/to/file.cpp:10:5: warning: This is a warning message';

    gccParser.parseLine(line, LineSource.stderr, fileReport);

    expect(fileReport).toHaveBeenCalledWith({
      severity: ReportSeverity.Minor,
      description: 'This is a warning message',
      line: 10,
      file: 'C:/path/to/file.cpp',
    });
  });

  it('ignores note messages', () => {
    const line = '/path/to/file.cpp:10:5: note: This is a note message';

    gccParser.parseLine(line, LineSource.stderr, fileReport);

    expect(fileReport).not.toHaveBeenCalled();
  });

  it('also handles GCC messages without a column included', () => {
    const line = '/path/to/file.cpp:10: warning: This is a warning message';

    gccParser.parseLine(line, LineSource.stderr, fileReport);

    expect(fileReport).toHaveBeenCalledWith({
      severity: ReportSeverity.Minor,
      description: 'This is a warning message',
      line: 10,
      file: '/path/to/file.cpp',
    });
  });

  it('can handle clang-tidy messages', () => {
    const line =
      '/path/to/file.cpp:54:17: error: use of a signed integer operand with a binary bitwise operator [hicpp-signed-bitwise,-warnings-as-errors]';

    gccParser.parseLine(line, LineSource.stdin, fileReport);

    expect(fileReport).toHaveBeenCalledWith({
      severity: ReportSeverity.Major,
      description:
        'use of a signed integer operand with a binary bitwise operator [hicpp-signed-bitwise,-warnings-as-errors]',
      line: 54,
      file: '/path/to/file.cpp',
    });
  });

  it('can handle clang-format --dry-run --Werror messages', () => {
    const line =
      'main/src/main.cpp:101:93: error: code should be clang-formatted [-Wclang-format-violations]';

    gccParser.parseLine(line, LineSource.stdin, fileReport);

    expect(fileReport).toHaveBeenCalledWith({
      severity: ReportSeverity.Major,
      description: 'code should be clang-formatted [-Wclang-format-violations]',
      line: 101,
      file: 'main/src/main.cpp',
    });
  });
});
