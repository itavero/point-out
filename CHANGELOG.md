# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0-alpha.2] - 2024-02-19

### Fixed

- Version number shown in help and `-V` was incorrect. Using version from `package.json` now.

## [0.1.0-alpha.1] - 2024-02-19

### Fixed

- Fixed packaging by auto-generating the .npmignore file.. hopefully. 🤞

## [0.1.0-alpha.0] - 2024-02-19

### Added

- Parser for CMake, Cpplint and GCC warnings/errors (not fully tested yet)
