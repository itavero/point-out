# 👉 point-out

Runs a given command and scans the output for warnings/errors to create a GitLab CI Code Quality report.

While running the command, the output is forwarded to `stdout`/`stderr` so you can still see what's happening.
At the same time, the output is parsed for warnings and errors, which are then used to create a Code Quality report in the GitLab CI pipeline.

Currently parsers for the following tools are implemented:

- CMake (warning and fatal error messages)
- GCC, and tools that can use the same message format, like:
  - clang-format
  - clang-tidy
- cpplint (using `emacs` format, which is its default)

## Usage

You can run `point-out -h` to see the latest usage/help information (which is probably more up-to-date than this README file).

```sh
point-out [options] -- [command to parse output from]
```

You can use the following options:

- `-h`, `--help`: display the help
- `-V`, `--version`: output the version number
- `-l`, `--list`: list all available parsers (currently not very useful, as all of them are always active)
- `-n`, `--no-append`: if set then report will be overwritten, instead of appending to it
- `-j`, `--json-indent <num>`: indent level for JSON output (default: "2")
- `-r`, `--repo-path <path>`: path to the repository (default: `CI_PROJECT_DIR` or current working directory)
- `-a`, `--no-relative`: do not make absolute paths relative to the repository path

## Current state

The tool is still a work in progress, but should already be somewhat useable.
If you run into any issues, please let me know.
