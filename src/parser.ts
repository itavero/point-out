/**
 * Indicates the severity of a report.
 */
export enum ReportSeverity {
  Info = 'info',
  Minor = 'minor',
  Major = 'major',
  Critical = 'critical',
  Blocker = 'blocker',
}

/**
 * A report from a analysis tool.
 */
export interface Report {
  description: string;
  severity: ReportSeverity;
  file: string;
  fingerprint?: string;
  line?: number;
}

export interface FileReport {
  (report: Report): void;
}

export enum LineSource {
  stdin,
  stderr,
}

/**
 * A parser that parsers the output of a analysis tool on a line-by-line basis.
 */
export interface Parser {
  get parserKey(): string;
  parseLine(line: string, source: LineSource, fileReport: FileReport): void;
  endOfStream(source: LineSource, fileReport: FileReport): void;
}
