import {Command} from 'commander';
import {exit} from 'node:process';
import {spawn} from 'node:child_process';
import CmakeParser from './parsers/cmake_parser';
import {LineSource, Report} from './parser';
import {createHash} from 'node:crypto';
import * as path from 'node:path';
import * as fs from 'node:fs';
import GccParser from './parsers/gcc_parser';
import {CqModel} from './cq_model';
import CpplintParser from './parsers/cpplint_parser';
import {getVersion} from './version';

// Default repository path is the CI_PROJECT_DIR environment variable, or the current working directory.
const defaultRepoPath = process.env.CI_PROJECT_DIR ?? process.cwd();

const appVersion = getVersion();
const program = new Command();
program
  .name('point-out')
  .addHelpText('before', `\u{1F449} point-out v${appVersion}`)
  .description(
    'Runs the given command and scans its output for warnings/errors to create a GitLab CI Code Quality report.'
  )
  .version(appVersion)
  .usage('[options] -- [command to parse output from]')
  .option(
    '-o, --output <path>',
    'report output path',
    'gl-code-quality-report.json'
  )
  .option('-n, --no-append', 'if set then report will be overwritten')
  .option('-j, --json-indent <num>', 'indent level for JSON output', '2')
  .option('-l, --list', 'list all available parsers')
  .option('-r, --repo-path <path>', 'path to the repository', defaultRepoPath)
  .option('-a, --no-relative', 'do not make absolute paths relative')
  .parse(process.argv);

const options = program.opts();

// Parsers
const parsers = [new CmakeParser(), new GccParser(), new CpplintParser()];

if (options.list) {
  console.log('Available parsers:');
  for (const parser of parsers) {
    console.log(`\t${parser.parserKey}`);
  }
  exit(0);
}

const ext_command = program.args;
if (ext_command === undefined || ext_command.length === 0) {
  console.error('No command to parse output from was provided');
  exit(1);
}
console.log(`\u{1F449} ${ext_command.join(' ')}\r\n`);

// Reports
const reports = new Map<string, Report[]>();
for (const parser of parsers) {
  reports.set(parser.parserKey, []);
}

function callParsers(source: LineSource, line: string) {
  parsers.forEach(parser => {
    parser.parseLine(line, source, report => {
      reports.get(parser.parserKey)?.push(report);
    });
  });
}

function callParsersOnEnd(source: LineSource) {
  parsers.forEach(parser => {
    parser.endOfStream(source, report => {
      reports.get(parser.parserKey)?.push(report);
    });
  });
}

// Start the given command and redirect it's output and error output
// so we can parse it line by line and also print it to the appropriate
// output/error stream of our script.
let line_out = '';
let line_err = '';
const proc = spawn(ext_command[0], ext_command.slice(1));
proc.stdout.on('data', data => {
  process.stdout.write(data);

  line_out += data.toString();
  let line_break = line_out.indexOf('\n');
  while (line_break !== -1) {
    const line = line_out.substring(0, line_break);
    callParsers(LineSource.stdin, line);
    line_out = line_out.substring(line_break + 1);
    line_break = line_out.indexOf('\n');
  }
});
proc.stderr.on('data', data => {
  process.stderr.write(data);

  line_err += data.toString();
  let line_break = line_err.indexOf('\n');
  while (line_break !== -1) {
    const line = line_err.substring(0, line_break);
    callParsers(LineSource.stderr, line);
    line_err = line_err.substring(line_break + 1);
    line_break = line_err.indexOf('\n');
  }
});

// Make file paths relative to the repository path
function makeFilePathRelative(file: string, root: string): string {
  // If it is already relative, return as-is
  if (!path.isAbsolute(file)) {
    return file;
  }

  // Determine path to file relative to the root.
  // Only return the relatiev path, if the file is within the root or a subdirectory.
  const relative = path.relative(root, file);
  if (!relative.startsWith('..')) {
    return relative;
  }

  // If the file is not within the root, return the absolute path.
  return file;
}

// Transform the reports into the format expected by GitLab CI Code Quality
function transformReport(checkKey: string, report: Report): CqModel {
  // For the fingerprint, use the fingerprint data from the report
  // as well as the file name and the check key.
  const hash = createHash('sha256');
  const filePath = options.noRelative
    ? report.file
    : makeFilePathRelative(report.file, options.repoPath);
  hash.update(checkKey);
  hash.update(filePath);
  if (report.fingerprint !== undefined && report.fingerprint.length > 0) {
    hash.update(report.fingerprint);
  } else {
    // Use description with whitespace removed
    hash.update(report.description.replace(/\s/g, ''));
  }
  // Create hexadecimal digest string from hash
  const fp = (checkKey + ':' + hash.digest('hex')).toLowerCase();
  return {
    check_name: checkKey,
    description: report.description.trimEnd(),
    fingerprint: fp,
    location: {
      path: filePath,
      lines: {
        begin: report.line ?? 0,
      },
    },
    severity: report.severity,
  };
}

proc.on('exit', code => {
  if (line_out.length > 0) {
    callParsers(LineSource.stdin, line_out);
  }
  callParsersOnEnd(LineSource.stdin);
  if (line_err.length > 0) {
    callParsers(LineSource.stderr, line_err);
  }
  callParsersOnEnd(LineSource.stderr);

  // Create JSON report
  const report_items: CqModel[] = [];
  for (const [parserKey, parserReports] of reports) {
    for (const report of parserReports) {
      const output = transformReport(parserKey, report);

      // Check if the fingerprint is not present yet in report_items.
      if (
        report_items.findIndex(
          item => item.fingerprint === output.fingerprint
        ) < 0
      ) {
        // New fingerprint. Add it to the report items.
        report_items.push(output);
        continue;
      }

      // Append line number to fingerprint if this is not present yet in report_items.
      const adjusted_fingerprint =
        output.fingerprint + ':' + output.location.lines.begin;
      if (
        report_items.findIndex(
          item => item.fingerprint === adjusted_fingerprint
        ) < 0
      ) {
        // Add with adjusted fingerprint
        output.fingerprint = adjusted_fingerprint;
        report_items.push(output);
        continue;
      }

      // Duplication was not resolved. For now ignore this report.
    }
  }

  // Check if duplicate fingerprints are present in report_items

  // Export the report
  if (fs.existsSync(options.output) && !options.noAppend) {
    // Add existing report items to the report items collected from the parsers
    const fileContent = fs.readFileSync(options.output, 'utf8');
    const existingReport = JSON.parse(fileContent);
    report_items.push(...existingReport);
  }
  fs.writeFileSync(
    options.output,
    JSON.stringify(report_items, null, parseInt(options.jsonIndent, 10))
  );

  // Exit with same exit code
  exit(code ?? 0);
});
