import {
  FileReport,
  LineSource,
  Parser,
  Report,
  ReportSeverity,
} from '../parser';

/**
 * Parses warning/error/note messages from cpplint (using the emacs format)
 */
class CpplintParser implements Parser {
  // Regex to match the output of cpplint in emacs mode, in which case it uses the following Python code:
  // (f'{filename}:{linenum}: '
  //   f' {message}  [{category}] [{confidence}]\n')
  private static messageRegex =
    /^(([A-Z]:[\\/])?[^:]+):(\d+):\s+(.*)\s+\[([^\]]+)\]\s+\[([^\]]+)\]\s*$/;

  get parserKey(): string {
    return 'cpplint';
  }

  parseLine(line: string, _source: LineSource, fileReport: FileReport) {
    // We don't care about the source, we parse the line regardless of its origin.
    const match = line.match(CpplintParser.messageRegex);
    if (match === null) {
      return;
    }
    const [, filename, , lineNo, message, category, confidence] = match;

    // Transform confidence to a severity
    const confidenceNumber = parseInt(confidence, 10);
    let severity: ReportSeverity;
    switch (confidenceNumber) {
      case 5:
        severity = ReportSeverity.Critical;
        break;
      case 4:
        severity = ReportSeverity.Major;
        break;
      case 3:
        severity = ReportSeverity.Minor;
        break;
      default:
        severity = ReportSeverity.Info;
        break;
    }

    const report: Report = {
      file: filename,
      line: parseInt(lineNo, 10),
      description: `${message.trim()} [${category}]`,
      severity: severity,
    };
    fileReport(report);
  }

  endOfStream(_source: LineSource, _fileReport: FileReport) {
    // Nothing to do
  }
}

export default CpplintParser;
