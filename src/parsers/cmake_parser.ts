import {
  FileReport,
  LineSource,
  Parser,
  Report,
  ReportSeverity,
} from '../parser';

/**
 * Parses CMake warning and error messages.
 */
class CmakeParser implements Parser {
  report: Report | undefined;
  initialWhitespaceCount: number | undefined;
  emptyLineCount = 0;

  get parserKey(): string {
    return 'cmake';
  }

  parseLine(line: string, source: LineSource, fileReport: FileReport) {
    if (source !== LineSource.stderr) {
      // From our testing, it seems that CMake only outputs warnings and errors
      // on the stderr stream.
      return;
    }

    const regex = /^CMake (Warning|Error) at (.+):(\d+) \(message\):/;
    const match = regex.exec(line);
    if (match !== null) {
      if (this.report !== undefined) {
        // Assume that the previous report is finished.
        this.wrapUpAndFileReportIfNeeded(fileReport);
      }

      this.report = {
        fingerprint: '',
        description: '',
        severity:
          match[1] === 'Warning'
            ? ReportSeverity.Minor
            : ReportSeverity.Blocker,
        file: match[2],
        line: parseInt(match[3]),
      };
      this.emptyLineCount = 0;
    } else {
      if (this.report !== undefined) {
        if (line.trim().length === 0) {
          this.emptyLineCount++;
          if (this.emptyLineCount >= 2) {
            // Assume that the previous report is finished.
            this.wrapUpAndFileReportIfNeeded(fileReport);
          }
        } else {
          const spacesAtBeginning = line.match(/^ +/)?.[0].length ?? 0;
          if (
            this.initialWhitespaceCount === undefined ||
            spacesAtBeginning < this.initialWhitespaceCount
          ) {
            // Count spaces at the beginning of the line.
            this.initialWhitespaceCount = spacesAtBeginning;
          }
          // Remove up to initialWhitespaceCount spaces from the beginning of line
          // before adding it to the description.

          // Assume that the previous report is not finished yet.
          this.report.description +=
            line.slice(this.initialWhitespaceCount) + '\n';
        }
      }
    }
  }

  endOfStream(source: LineSource, fileReport: FileReport) {
    if (source !== LineSource.stderr) {
      // From our testing, it seems that CMake only outputs warnings and errors
      // on the stderr stream.
      return;
    }
    this.wrapUpAndFileReportIfNeeded(fileReport);
  }

  wrapUpAndFileReportIfNeeded(fileReport: FileReport) {
    if (this.report !== undefined) {
      this.report.description = this.report.description.trimEnd();
      fileReport(this.report);
      this.initialWhitespaceCount = undefined;
      this.report = undefined;
    }
  }
}

export default CmakeParser;
