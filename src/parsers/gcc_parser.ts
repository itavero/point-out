import {
  FileReport,
  LineSource,
  Parser,
  Report,
  ReportSeverity,
} from '../parser';

/**
 * Parses warning/error/note messages from the GCC compiler and linker.
 */
class GccParser implements Parser {
  private static messageRegex =
    /^(([A-Z]:[\\/])?[^:]+):(\d+):(\d+:)? (\w+): (.*)$/;

  get parserKey(): string {
    return 'gcc';
  }

  parseLine(line: string, _source: LineSource, fileReport: FileReport) {
    // We don't care about the source, we parse the line regardless of its origin.
    const match = line.match(GccParser.messageRegex);
    if (match === null) {
      return;
    }
    const [, filename, , lineNo, , type, message] = match;

    // Ignore notes
    if (type === 'note') {
      return;
    }

    let severity = ReportSeverity.Info;
    if (type === 'error') {
      severity = ReportSeverity.Major;
    } else if (type === 'warning') {
      severity = ReportSeverity.Minor;
    }

    const report: Report = {
      file: filename,
      line: parseInt(lineNo, 10),
      description: message.trim(),
      severity: severity,
    };
    fileReport(report);
  }

  endOfStream(_source: LineSource, _fileReport: FileReport) {
    // Nothing to do
  }
}

export default GccParser;
