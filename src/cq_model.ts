export class CqLocation {
  path!: string;
  lines!: CqLines;
}

export class CqLines {
  begin!: number;
}

export class CqModel {
  description!: string;
  check_name!: string;
  fingerprint!: string;
  severity!: string;
  location!: CqLocation;
}
